﻿using HomeWork_Stage4_Lesson4.Entities;
using HomeWork_Stage4_Lesson4.FilterModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork_Stage4_Lesson4.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GroupsController : ControllerBase
    {
        [HttpGet("{id}")]
        public IActionResult GetGroup(long id)
        {
            Array groups;
            using(var db = new BasicDbContext())
            {
                groups = db.Groups.Where(x => x.Id == id)
                                  .Select(x => new { x.Course.CourseName, x.StartDate, x.EndDate, x.People })
                                  .ToArray();
            }
            return Ok(groups);
        }
        [HttpGet]
        public IActionResult GetGroup([FromQuery]GroupFilterModel groupFilterModel)
        {
            Array groups;
            using(var db = new BasicDbContext())
            {
                var query = db.Groups.Select(x => new
                {
                    x.Course.CourseName,
                    x.StartDate,
                    x.EndDate,
                    x.CourseId,
                    People = x.People.Select(x => new 
                    { 
                        x.FirstName, 
                        x.LastName, 
                        Status = x.Status.ToString() 
                    }) 
                }).AsQueryable();
                if (groupFilterModel.StartYear.HasValue)
                {
                    query = query.Where(x => x.StartDate.Year == groupFilterModel.StartYear);
                }
                if (groupFilterModel.StartMonth.HasValue)
                {
                    query = query.Where(x => x.StartDate.Month == groupFilterModel.StartMonth);
                }
                if (groupFilterModel.EndYear.HasValue)
                {
                    query = query.Where(x => x.EndDate.Year == groupFilterModel.EndYear);
                }
                if (groupFilterModel.EndMonth.HasValue)
                {
                    query = query.Where(x => x.EndDate.Month == groupFilterModel.EndMonth);
                }
                if (groupFilterModel.CourseId.HasValue)
                {
                    query = query.Where(x => x.CourseId == groupFilterModel.CourseId);
                }
                groups = query.Select(x => new { x.CourseName, x.StartDate, x.EndDate, x.People }).ToArray();
            }
            return Ok(groups);
        }
        [HttpPost]
        public IActionResult AddCourse([FromBody] Group group)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            using(var db = new BasicDbContext())
            {
                db.Groups.Add(group);
                db.SaveChanges();
            }
            return Created("The data has been successfully saved", group);
        }
        [HttpPut("{id}")]
        public IActionResult UpdateGroup([FromRoute] long id, [FromBody] Group group)
        {
            using (var db = new BasicDbContext())
            {
                var groupForUpdate = db.Groups.Find(id);
                if (groupForUpdate == null || group.IsAllValuesDefaultOrNull)
                {
                    return BadRequest("Wrong GroupId");
                }
                if (group.StartDate != default)
                {
                    groupForUpdate.StartDate = group.StartDate;
                }
                if (group.EndDate != default)
                {
                    groupForUpdate.EndDate = group.EndDate;
                }
                if (group.CourseId != default)
                {
                    groupForUpdate.CourseId = group.CourseId;
                }                
                db.Groups.Update(groupForUpdate);
                db.SaveChanges();
            }
            return Ok();
        }
        [HttpDelete("{id}")]
        public IActionResult DeleteGroup(long id)
        {
            using(var db = new BasicDbContext())
            {
                var groupForDelete = db.Groups.Find(id);
                if (groupForDelete == null)
                {
                    return BadRequest("Wrong GroupId");
                }
                db.Groups.Remove(groupForDelete);
                db.SaveChanges();
            }
            return Ok();
        }
    }
}
