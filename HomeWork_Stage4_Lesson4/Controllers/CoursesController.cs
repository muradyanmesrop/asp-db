﻿using HomeWork_Stage4_Lesson4.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork_Stage4_Lesson4.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CoursesController: ControllerBase
    {
        [HttpGet("{id}")]
        public IActionResult GetCorses(long id)
        {
            Array courses;
            using (var db = new BasicDbContext())
            {
                courses = db.Courses.Where(x => x.Id == id).ToArray();
            }
            return Ok(courses);
        }
        [HttpGet]
        public IActionResult GetCorses()
        {
            Array courses;
            using (var db = new BasicDbContext())
            {
                courses = db.Courses.ToArray();
            }
            return Ok(courses);
        }
        [HttpPost]
        public IActionResult AddCourse([FromBody] Course course)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            using (var db = new BasicDbContext())
            {
                db.Courses.Add(course);
                db.SaveChanges();
            }
            return Created("The data has been successfully saved", course);
        }
        [HttpPut("{id}")]
        public IActionResult UpdateCourse([FromRoute] long id, [FromBody] Course course)
        {
            using (var db = new BasicDbContext())
            {
                var courseForEdit = db.Courses.Find(id);
                if (courseForEdit == null || !ModelState.IsValid)
                {
                    return BadRequest();
                }
                courseForEdit.CourseName = course.CourseName;
                db.Courses.Update(courseForEdit);
                db.SaveChanges();
            }
            return Ok();
        }
        [HttpDelete("{id}")]
        public IActionResult DeleteCourse(long id)
        {
            using (var db = new BasicDbContext())
            {
                var courseForDelete = db.Courses.Find(id);
                if (courseForDelete == null)
                {
                    return BadRequest("Wrong CourseId");
                }
                db.Courses.Remove(courseForDelete);
                db.SaveChanges();
            }
            return Ok();
        }
    }
}
