﻿using HomeWork_Stage4_Lesson4.Entities;
using HomeWork_Stage4_Lesson4.FilterModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork_Stage4_Lesson4.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PeopleController : ControllerBase
    {        
        [HttpGet]
        public IActionResult GetPerson([FromQuery]PersonFilterModel person)
        {
            Array students;
            using (var db = new BasicDbContext())
            {
                var query = db.People.AsQueryable();
                if (person.Id.HasValue)
                {
                    query = query.Where(x => x.Id == person.Id);
                }
                if (!string.IsNullOrEmpty(person.FirstName))
                {
                    query = query.Where(x => x.FirstName.Contains(person.FirstName));
                }
                if (!string.IsNullOrEmpty(person.LastName))
                {
                    query = query.Where(x => x.LastName.Contains(person.LastName));
                }
                if (!string.IsNullOrEmpty(person.Country))
                {
                    query = query.Where(x => x.Country.Contains(person.Country));
                }
                if (person.BirthYear.HasValue)
                {
                    query = query.Where(x => x.BirthDate.Year == person.BirthYear);
                }
                if (person.GroupId.HasValue)
                {
                    query = query.Where(x => x.GroupId == person.GroupId);
                }
                if (person.Status.HasValue)
                {
                    query = query.Where(x => x.Status == person.Status);
                }
                students = query.Select(x => new { x.FirstName, x.LastName, x.BirthDate, x.Country, x.City, x.Group.Course.CourseName, Status = x.Status.ToString() })
                            .ToArray();
            }
            return Ok(students);
        }
        [HttpGet("{id}")]
        public IActionResult GetPerson(long id)
        {
            Array students;
            using(var db = new BasicDbContext())
            {
                students = db.People.Where(x=>x.Id == id)
                                    .Select(x => new { x.FirstName, x.LastName, x.BirthDate, x.Country, x.City, x.Group.Course.CourseName, Status = x.Status.ToString() })
                                    .ToArray();
            }
            return Ok(students);
        }
        [HttpPost]
        public IActionResult AddPeople([FromBody] Person person)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            using (var db = new BasicDbContext())
            {
                db.People.Add(person);
                db.SaveChanges();
            }
            return Created("", person);
        }
        [HttpPut("{id}")]
        public IActionResult UpdatePeople([FromRoute] long id, [FromBody] Person person)
        {
            using (var db = new BasicDbContext())
            {
                var personForEdit = db.People.Find(id);
                if (personForEdit == null || person.IsAllValuesDefaultOrNull)
                {
                    return BadRequest("Wrong StudentId");
                }
                if (person.FirstName != null)
                {
                    personForEdit.FirstName = person.FirstName;
                }
                if (person.LastName != null)
                {
                    personForEdit.LastName = person.LastName;
                }
                if (person.BirthDate != default)
                {
                    personForEdit.BirthDate = person.BirthDate;
                }
                if (person.Country != null)
                {
                    personForEdit.Country = person.Country;
                }
                if (person.City != null)
                {
                    personForEdit.City = person.City;
                }
                if (person.Status != default)
                {
                    personForEdit.Status = person.Status;
                }
                if (person.GroupId != default)
                {
                    personForEdit.GroupId = person.GroupId;
                }
                db.People.Update(personForEdit);
                db.SaveChanges();
            }
            return Ok();
        }
        [HttpDelete("{id}")]
        public IActionResult DeletePeople(long id)
        {
            using (var db = new BasicDbContext())
            {
                var personForDelete = db.People.Find(id);
                if (personForDelete == null)
                {
                    return BadRequest("Wrong StudentId");
                }
                db.People.Remove(personForDelete);
                db.SaveChanges();
            }
            return Ok();
        }
    }
}
