﻿using HomeWork_Stage4_Lesson4.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork_Stage4_Lesson4
{
    public class BasicDbContext : DbContext
    {
        public DbSet<Person> People { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Group> Groups { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server = DESKTOP-MY; Database = BasicDB; Trusted_Connection = true");
            base.OnConfiguring(optionsBuilder);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Course>(x =>
            {
                x.ToTable("Courses");
                x.HasKey(x => x.Id);
                x.Property(x => x.Id).ValueGeneratedOnAdd();
                x.Property(x => x.CourseName).HasColumnType("NVARCHAR(50)");
            });
            modelBuilder.Entity<Group>(x =>
            {
                x.ToTable("Groups");
                x.HasKey(x => x.Id);
                x.Property(x => x.Id).ValueGeneratedOnAdd();
                x.Property(x => x.StartDate).HasColumnType("DATETIME").IsRequired();
                x.Property(x => x.EndDate).HasColumnType("DATETIME");
                x.HasOne(x => x.Course).WithMany(x => x.Groups).HasForeignKey(x => x.CourseId).HasPrincipalKey(x => x.Id);
            });
            modelBuilder.Entity<Person>(x =>
            {
                x.HasKey(x => x.Id);
                x.Property(x => x.Id).ValueGeneratedOnAdd();
                x.Property(x => x.FirstName).HasColumnType("NVARCHAR").IsRequired();
                x.Property(X => X.LastName).HasColumnType("NVARCHAR").IsRequired();
                x.Property(x => x.BirthDate).HasColumnType("DATETIME").IsRequired();
                x.Property(x => x.Status).IsRequired();
                x.Property(x => x.Country).HasColumnType("NVARCHAR");
                x.Property(x => x.City).HasColumnType("NVARCHAR");
                x.HasOne(x => x.Group).WithMany(x => x.People).HasForeignKey(x => x.GroupId).HasPrincipalKey(x => x.Id);
            });
            modelBuilder.Entity<Person>(x =>
            {
                x.Property(x => x.FirstName).HasColumnType("NVARCHAR(50)").IsRequired();
                x.Property(X => X.LastName).HasColumnType("NVARCHAR(50)").IsRequired();
                x.Property(x => x.Country).HasColumnType("NVARCHAR(50)").IsRequired(false);
                x.Property(x => x.City).HasColumnType("NVARCHAR(50)").IsRequired(false);
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}
