﻿using HomeWork_Stage4_Lesson4.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork_Stage4_Lesson4.FilterModels
{
    public class PersonFilterModel
    {
        public long? Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public int? BirthYear { get; set; }
        public string City { get; set; }
        public StatusP? Status { get; set; }
        public long? GroupId { get; set; }
    }
}
