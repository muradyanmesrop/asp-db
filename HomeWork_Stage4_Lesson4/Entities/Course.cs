﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork_Stage4_Lesson4.Entities
{
    public class Course
    {
        public long Id { get; set; }
        public string CourseName { get; set; }
        public List<Group> Groups { get; set; }
    }
}
