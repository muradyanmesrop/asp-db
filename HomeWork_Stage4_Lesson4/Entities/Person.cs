﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork_Stage4_Lesson4.Entities
{
    public class Person
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public DateTime BirthDate { get; set; }
        public StatusP Status { get; set; }
        public long GroupId { get; set; }
        public Group Group { get; set; }

        public bool IsAllValuesDefaultOrNull
        {
            get
            {
                return FirstName == null &&
                       LastName == null &&
                       BirthDate == default &&
                       Country == null &&
                       City == null &&
                       Status == default &&
                       GroupId == default;
            }
        }
    }
}
