﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork_Stage4_Lesson4.Entities
{
    public class Group
    {
        public long Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public long CourseId { get; set; }
        public Course Course { get; set; }
        public List<Person> People { get; set; }

        public bool IsAllValuesDefaultOrNull
        {
            get
            {
                return EndDate == default &&
                       StartDate == default &&
                       CourseId == default;
            }
        }
    }
}
